import { useContext, useEffect, useState } from "react";
import { Button, Table, Form, Modal } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";



import UserContext from "../UserContext";




export default function AdminDashboard() {



    const { user } = useContext(UserContext);

//addnewproduct
    const Navigate = useNavigate();

    function addnewproduct() {
        

        Navigate("/addproduct");
    }
    //addnewproduct

// edit product
    // State hooks to store the values of the input fields for our modal.
    const [productItemId, setProductItemId] = useState("");
    const [productItemName, setProductItemName] = useState("");
    const [productItemDescription, setProductItemDescription] = useState("");
    const [productItemPrice, setProductItemPrice] = useState(0);
    const [productItemStocks, setProductItemStocks] = useState(0);

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
    const [showEdit, setShowEdit] = useState(false);

    // To control the add course modal pop out
    const openAdd = () => setShowAdd(true); //Will show the modal
    const closeAdd = () => setShowAdd(false); //Will hide the modal

    // To control the edit course modal pop out
    // We have passed a parameter from the edit button so we can retrieve a specific course and bind it with our input fields.
    const openEdit = (id) => {
        setProductItemId(id);

        // Getting a specific course to pass on the edit modal
        fetch(`${process.env.REACT_APP_API_URL}/productitems/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                // updating the course states for editing
                setProductItemName(data.productItemName);
                setProductItemDescription(data.productItemDescription);
                setProductItemPrice(data.productItemPrice);
                setProductItemStocks(data.productItemStocks);
            });

        setShowEdit(true)
    };

    const closeEdit = () => {

        // Clear input fields upon closing the modal
        setProductItemName('');
        setProductItemDescription('');
        setProductItemPrice(0);
        setProductItemStocks(0);

        setShowEdit(false);
    };

// edit product

    // Create allProducts state to contain all the courses from the response of our fetc hData
    const [allProducts, setAllProducts] = useState([]);

    // [SECTION] Setting the course to Active/Inactive
    // Making the course inactive
    const archive = (productItemId, productItemName) => {
        console.log(productItemId);
        console.log(productItemName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/productitems/archive/${productItemId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productItemIsActive: false
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data) {
                    Swal.fire({
                        title: "Archive Successful",
                        icon: "success",
                        text: `${productItemName} is now inactive.`
                    });
                    // To show the update with the specific operation intiated.
                    fetchData();
                }
                else {
                    Swal.fire({
                        title: "Archive unsuccessful",
                        icon: "error",
                        text: "Something went wrong. Please try again later!"
                    });
                }
            })
    }




    // Making the course active
    const unarchive = (productItemId, productItemName) => {
        console.log(productItemId);
        console.log(productItemName);

        // Using the fetch method to set the isActive property of the course document to false
        fetch(`${process.env.REACT_APP_API_URL}/productitems/archive/${productItemId}`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productItemIsActive: true
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data)

                if (data) {
                    Swal.fire({
                        title: "Unarchive Successful",
                        icon: "success",
                        text: `${productItemName} is now active.`
                    });
                    // To show the update with the specific operation intiated.
                    fetchData();
                }
                else {
                    Swal.fire({
                        title: "Unarchive Unsuccessful",
                        icon: "error",
                        text: "Something went wrong. Please try again later!"
                    });
                }
            })
    }


    //fetchData() function to get all the active/inactive courses.
    const fetchData = () => {
        // get all the courses from the database
        fetch(`${process.env.REACT_APP_API_URL}/productitems/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                setAllProducts(data.map(product => {
                    return (
                        <tr key={product._id}>
                            <td>
                                {product._id}
                            </td>
                            <td>
                                {product.productItemName}
                            </td>
                            <td>
                                {product.productItemDescription}
                            </td>
                            <td>
                                {product.productItemPrice}
                            </td>
                            <td>
                                {product.productItemStocks}
                            </td>
                            <td>
                                {product.productItemIsActive ? "Active" : "Inactive"}
                            </td>
                            <td>

                                {
                                    //conditional rendering on what button should be visible base on the status of the course
                                    (product.productItemIsActive)
                                        ?
                                        <Button variant="danger" size="small" onClick={() =>
                                            archive(product._id, product.productItemName)
                                        }>
                                            Archive
                                        </Button>
                                        :
                                        <>
                                            <Button variant="success" size="small" className="mx-1" onClick={() => 
                                            unarchive(product._id, product.productItemName)
                                            }>
                                                Unarchive
                                            </Button>

                                           
                                            <Button variant="secondary" size="sm" className="mx-1" onClick={() => 
                                            openEdit(product._id)
                                            }>
                                                Edit
                                            </Button>
                                        </>
                                }
                            </td>




                        </tr>
                    )
                }));
            });
    }


    // To fetch all courses in the first render of the page
    useEffect(() => {
        fetchData();
    }, [])

 
    // EDIT SPECIFIC PRODUCT
    
    // Updating a specific course in our database
    // edit a specific course
    const modifyProduct = (e) => {
        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/productitems/${productItemId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                productItemName: productItemName,
                productItemDescription: productItemDescription,
                productItemPrice: productItemPrice,
                productItemStocks: productItemStocks
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "Product succesfully Updated",
                        icon: "success",
                        text: `${productItemName} is now updated`
                    });

                    // To automatically add the update in the page
                    fetchData();
                    // Automatically closed the form
                    closeEdit();

                }
                else {
                    Swal.fire({
                        title: "Error!",
                        icon: "error",
                        text: `Something went wrong. Please try again later!`
                    });

                    closeEdit();
                }

            })

        // Clear input fields
        setProductItemName('');
        setProductItemDescription('');
        setProductItemPrice(0);
        setProductItemStocks(0);
    } 

    // submit button for edit course
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if (productItemName != "" && productItemDescription != "" && productItemPrice > 0 && productItemStocks > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productItemName, productItemDescription, productItemPrice, productItemStocks]);
    // EDIT SPECIFIC PRODUCT

    return (


        (UserContext)
            ?
            <>
                {/* Header for the admin dashboard and functionality for create course and show enrollments */}
                <div className="mt-5 mb-3 text-center">
                    <h1>Admin Dashboard</h1>

                    {/* Adding a new course onClick={() => addproduct()
                    }*/}
                    <Button variant="success" className="mx-2" onClick={()=>addnewproduct()}>
                        Add Product
                    </Button>

                    {/* To view all the user enrollments */}
                    <Button variant="secondary" className="mx-2">
                        Retrieve All
                    </Button>



                </div>

                {/* For view all the courses in the database */}
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Stocks</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allProducts}
                    </tbody>
                </Table>



                {/*Modal for Editing a course*/}
                <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
                    <Form onSubmit={e => modifyProduct(e)}>

                        <Modal.Header closeButton>
                            <Modal.Title>Edit a Course</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <Form.Group controlId="name" className="mb-3">
                                <Form.Label>Enter New Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter the new product name"
                                    value={productItemName}
                                    onChange={e => setProductItemName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="description" className="mb-3">
                                <Form.Label>Enter New Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={3}
                                    placeholder="Enter the new product description"
                                    value={productItemDescription}
                                    onChange={e => setProductItemDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="price" className="mb-3">
                                <Form.Label>Enter New Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter the new product price"
                                    value={productItemPrice}
                                    onChange={e => setProductItemPrice(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="stocks" className="mb-3">
                                <Form.Label>Enter New Product Stocks</Form.Label>
                                <Form.Control
                                    type="number"
                                    placeholder="Enter the new product stocks"
                                    value={productItemStocks}
                                    onChange={e => setProductItemStocks(e.target.value)}
                                    required
                                />
                            </Form.Group>
                        </Modal.Body>

                        <Modal.Footer>
                            {isActive
                                ?
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Save
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" disabled>
                                    Save
                                </Button>
                            }
                            <Button variant="secondary" onClick={closeEdit}>
                                Close
                            </Button>
                        </Modal.Footer>

                    </Form>
                </Modal>
                {/*End of modal for editing a course*/}
            </>
            :
            <navigate to="/home" />
    )


}