// import Carousel from 'react-bootstrap/Carousel';
import { Image } from 'react-bootstrap';
export default function Banner() {
    return (
        <>
            <div>
                <Image
                    className="d-block"
                    src="/images/20220819_b_final.jpg"
                    fluid
                />
            </div>
            {/* <Carousel>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://images.unsplash.com/photo-1519458246479-6acae7536988?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80?text=First slide&bg=373940 "
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <p>“When words become unclear, I shall focus with photographs. When images become inadequate, I shall be content with silence.”</p>
                        <p>– Ansel Adams</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://images.pexels.com/photos/2929411/pexels-photo-2929411.jpeg?text=Second slide&bg=282c34"
                        alt="Second slide"
                    />

                    <Carousel.Caption>
                        <p>“Photography is the art of making memories tangible.”</p>
                        <p>– Destin Sparks</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="https://i0.wp.com/photofocus.com/wp-content/uploads/2020/02/xt4-featured.jpg?resize=1280%2C720&ssl=1?text=Third slide&bg=20232a"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <p>“Your photography is a record of your living, for anyone who really sees.”</p>
                        <p>– Paul Strand</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel> */}
        </>

    );
}
